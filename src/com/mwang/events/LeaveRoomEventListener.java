package com.mwang.events;

import com.mwang.model.Connection;
import com.mwang.model.Room;

public interface LeaveRoomEventListener {
    public void OnLeave(Room room, Connection connection);
}
