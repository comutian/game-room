package com.mwang.events;


import com.mwang.model.Connection;
import java.util.ArrayList;

public interface CreateRoomEvent {
    ArrayList<CreateRoomEventListener> createRoomListeners = new ArrayList();
    public void addCreateRoomEventListner(CreateRoomEventListener listener);
    public void invokeCreateRoomEvent(String name, Connection owner);
}
