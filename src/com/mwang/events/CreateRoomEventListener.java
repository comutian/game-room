package com.mwang.events;

import com.mwang.model.Connection;

public interface CreateRoomEventListener {
    public void OnCreate(String name, Connection connection);
}
