package com.mwang.events;

import com.mwang.model.Connection;
import com.mwang.model.Room;

import java.util.ArrayList;

public interface LeaveRoomEvent {
    ArrayList<LeaveRoomEventListener> leaveRoomListeners = new ArrayList();
    public void addLeaveRoomEventListener(LeaveRoomEventListener listener);
    public void invokeLeaveRoomEvent(Room room, Connection owner);
}
