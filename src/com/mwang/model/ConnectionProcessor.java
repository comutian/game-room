package com.mwang.model;

public class ConnectionProcessor extends Thread{
    Server server;
    public ConnectionProcessor(Server server) {
        this.server = server;
    }

    public void run() {
        while (true) {
            this.server.pending();
            try {
                //Laisser une chance d'exécution aux autres threads
                Thread.sleep(10);
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }
}

