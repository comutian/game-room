package com.mwang.model;

import com.mwang.events.CreateRoomEvent;
import com.mwang.events.CreateRoomEventListener;
import com.mwang.events.LeaveRoomEvent;
import com.mwang.events.LeaveRoomEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;


public abstract class Room implements CreateRoomEvent, LeaveRoomEvent {
    protected ArrayList<Connection> connections;
    protected String name;
    protected int maxConnections;
    protected Lock lock; // THIS LOCK IS SHARED BY ALL ROOMS

    //CONSTRUCTERS
    public Room() {
    }

    public Room(String name, int maxConnections) {
        this.name = name;
        this.maxConnections = maxConnections;
    }

    //GETTERS
    public ArrayList<Connection> getConnexions() {
        return connections;
    }

    public String getName() {
        return name;
    }

    public int getMaxConnections() {
        return maxConnections;
    }

    //ADD MULTIPLE CONNECTIONS FROM ROOM
    protected void addConnections(ArrayList<Connection> newConnections) {
        lock.lock();
        this.connections.addAll(newConnections);
        lock.unlock();
        for (Connection connection : newConnections) {
            connection.forwardMessage(new WebMessage(WebMessage.OK, name, "Bienvenue dans " + this.name + "."));
        }
    }

    //ADD ONE CONNECTION FROM ROOM
    protected void addConnection(Connection newConnection) {
        lock.lock();
        connections.add(newConnection);
        lock.unlock();
        newConnection.forwardMessage(new WebMessage(WebMessage.OK, name, "Bienvenue dans [" + this.name + "]"));
    }

    //REMOVE MULTIPLE CONNECTIONS FROM ROOM
    protected void removeConnections(ArrayList<Connection> closingConnections) {
        System.out.println(this.connections);
        lock.lock();
        this.connections.removeAll(closingConnections);
        lock.unlock();
        System.out.println(this.connections);
    }


    //REMOVE ONE CONNECTION FROM ROOM
    protected void removeConnection(Connection closingConnection) {
        ArrayList<Connection> temp = new ArrayList<>();
        temp.add(closingConnection);
        this.connections.removeAll(temp);
    }

    //READ MESSAGES OF EACH CONNECTIONS MADE WITH THIS ROOM
    protected void readMessages() {
        String text;
        ArrayList closedConnections = new ArrayList();
        lock.lock();
        try {
            for (Connection connexion : connections) {
                if (connexion.pendingMessage()) {
                    text = connexion.readMessage();
                    System.out.println("READMESSAGE " + text);
                    WebMessage message = WebMessage.parse(text);
                    try {
                        switch (message.getCode()) {
                            case WebMessage.CONTINUE: // 100
                                this.sendMessages(message, connections);
                                break;
                            case WebMessage.OK: // 200
                                this.sendMessages(new WebMessage(WebMessage.NEW_WEBUNITJOINED, this.name, message.getOwner()), connections);
                                break;
                            case WebMessage.QUIT:// 150
                                connexion.closeConnection();
                                closedConnections.add(connexion);
                                break;
                            case WebMessage.CREATE_SALON: // 104
                                this.removeConnection(connexion);
                                invokeCreateRoomEvent(message.getMessage(), connexion);

                        }
                    } catch (Exception ex) {
                        System.out.println();
                        ex.printStackTrace();
                    }
                }
            }
            if (closedConnections.size() > 0) {
                removeConnections(closedConnections);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            lock.unlock();
        }
    }


    //SEND MESSAGE TO EACH CONNECTION MADE WITH THIS ROOM
    protected void sendMessages(WebMessage message, ArrayList<Connection> connections) {
        for (Connection connection : connections) {
            connection.forwardMessage(message);
        }
    }


    //EVENT HANDLING FOR "SERVER"-SIDE ACTION
    @Override
    public void addCreateRoomEventListner(CreateRoomEventListener listener) {
        createRoomListeners.add(listener);
    }

    @Override
    public void invokeCreateRoomEvent(String name, Connection owner) {
        lock.lock();
        createRoomListeners.forEach((element) -> element.OnCreate(name, owner));
        lock.unlock();
    }

    @Override
    public void addLeaveRoomEventListener(LeaveRoomEventListener listener) {
        leaveRoomListeners.add(listener);
    }

    @Override
    public void invokeLeaveRoomEvent(Room room, Connection connection) {
        lock.lock();
        leaveRoomListeners.forEach((element) -> element.OnLeave(room, connection));
        lock.unlock();
    }
}