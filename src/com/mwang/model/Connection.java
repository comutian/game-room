package com.mwang.model;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Connection {
    private Socket interne;
    private PrintWriter os;
    private BufferedInputStream is;

    public Connection(ServerSocket serveurSock) throws IOException {
        interne = serveurSock.accept();
        os = new PrintWriter(interne.getOutputStream());
        is = new BufferedInputStream(interne.getInputStream());
    }


    public Socket getInterne() {
        return interne;
    }

    public void setInterne(Socket interne) {
        this.interne = interne;
    }

    public PrintWriter getOs() {
        return os;
    }

    public BufferedInputStream getIs() {
        return is;
    }

    //OTHER GETTERS AND SETTERS

    public int getPort(){
        return interne.getPort();
    }

    public void forwardMessage(WebMessage message) {
        if (os != null) {
            System.out.println("FOWARD "+ message);
            os.print(message);
            os.flush();
        }
    }

    public boolean pendingMessage() throws IOException {
        return is.available() > 0;
    }

    public String readMessage() throws IOException {
        String texte = null;
        if (is != null) {
            byte buffer[] = new byte[500];
            is.read(buffer);
            texte = (new String(buffer)).trim();
            buffer = null;
        }
        return texte;
    }

    public void closeConnection() throws IOException {
        os.close();
        is.close();
        interne.close();
        os = null;
        is = null;
        interne = null;
    }
}
