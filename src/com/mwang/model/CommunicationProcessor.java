package com.mwang.model;

class CommunicationProcessor extends Thread {
    Server server;

    public CommunicationProcessor(Server server) {
        this.server = server;
    }

    public void run() {
        while (true && !interrupted()) {
            server.read();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}