package com.mwang.model;

import com.mwang.events.CreateRoomEventListener;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;

public class LobbyRoom extends Room {
    public LobbyRoom(String name, int maxConnections, Lock lock) {
        super(name, maxConnections);
        this.connections = new ArrayList<>(maxConnections);
        this.lock = lock;
    }
}
