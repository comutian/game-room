package com.mwang.model;//Laboratoire 1 : CORRECTION - Le serveur

import com.mwang.events.CreateRoomEventListener;
import com.mwang.events.LeaveRoomEventListener;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Server extends WebUnit implements CreateRoomEventListener, LeaveRoomEventListener {
    private ServerSocket serveurSocket;
    private String name = "SERVER";

    //Rooms
    private ArrayList<GameRoom> rooms;
    private LobbyRoom lobby;

    //Threads
    private CommunicationProcessor communicationProcessor; //Thread d'inspection du serveur
    private ConnectionProcessor connectionProcessor;
    private Lock lock;

    public Server(int port) {
        this.port = port;
    }

    public Server() {
        this(5555);
    }

    //main() démarre le serveur:
    public static void main(String args[]) {
        Server server = new Server(5555);
        server.start();
    }

    private void start() {
        try {
            //Création du socket
            synchronized (this) {
                serveurSocket = new ServerSocket(port);
                lock = new ReentrantLock();
                rooms = new ArrayList<>();
                lobby = new LobbyRoom("Lobby", 9999, lock);
                lobby.addCreateRoomEventListner(this);
                connectionProcessor = new ConnectionProcessor(this);
                connectionProcessor.start();
                communicationProcessor = new CommunicationProcessor(this);
                communicationProcessor.start();
            }

            //Message à l'usager
            System.out.println("START SERVER ON " + this.getClass().getName() + serveurSocket);

        } catch (IOException e) {
            System.out.println("\nModele.Serveur déjà actif sur ce port...");
        }
    }

    @Override
    public WebMessage read() {
        try {
            lobby.readMessages();
            for (Room room : rooms) {
                room.readMessages();
            }
            return new WebMessage();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new WebMessage();
    }

    public void send(int code, String message) {

    }

    public void send(WebMessage message) {

    }

    public void pending() {
        try {
            Connection client = new Connection(serveurSocket);
            System.out.println("Connexion sur le port #" + client.getPort());
            // First connection
            lobby.addConnection(client);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private void createGameRoom(String name, Connection connection){
        GameRoom newGameRoom = new GameRoom(name,5,this.lock);
        newGameRoom.addConnection(connection);
        newGameRoom.addCreateRoomEventListner(this);
        lock.lock();
        this.rooms.add(newGameRoom);
        lock.unlock();
        System.out.println(lobby.connections);

    }

    @Override
    public void OnCreate(String name, Connection connection) {
        this.createGameRoom(name,connection);
    }

    @Override
    public void OnLeave(Room room, Connection connection) {
        room.removeConnection(connection);
        if(room.connections.isEmpty() && !(room instanceof LobbyRoom)){
            rooms.remove(room);
        }
        else if(room instanceof LobbyRoom){
            try {
                connection.closeConnection();
            } catch (IOException e) {
                System.out.println("Error! " + connection.getInterne() + "shutdown exception");
            }
        }
    }
}