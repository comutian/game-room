package com.mwang.model;

public class MessageProcessor extends Thread{
    Client client;
    public MessageProcessor(Client client) {
        this.client = client;
    }

    public void run() {
        while (true && !interrupted()) {
            //Lire le socket
            try {
                client.read();
            } catch (Exception x) {
                System.out.println("Exception dans thread" + x);
            }
        }
    }
}
