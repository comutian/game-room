package com.mwang.model;

import java.io.BufferedInputStream;
import java.io.PrintWriter;
import java.util.Observable;

public abstract class WebUnit extends Observable {
    protected String ip;
    protected int port;
    protected PrintWriter os; //Output Stream
    protected BufferedInputStream is; //Input Stream
    protected String alias; //Alias de l'usager
    private String status = ""; //État actuel du client


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
        setChanged();
        notifyObservers();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
        setChanged();
        notifyObservers();
    }

    public PrintWriter getOs() {
        return os;
    }

    public void setOs(PrintWriter os) {
        this.os = os;
    }

    public BufferedInputStream getIs() {
        return is;
    }

    public void setIs(BufferedInputStream is) {
        this.is = is;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
//        setChanged();
//        notifyObservers();
    }

    public abstract WebMessage read() throws Exception;
    public abstract void send(int code,String message);
    public abstract void send(WebMessage message);


}
