package com.mwang.model;

public class WebMessage {
    public final static int CONTINUE = 100;
    public final static int OK = 200;
    public final static int QUIT = 150;
    public final static int INTERNAL_SERVER_PROBLEM = 500;
    public final static int ROOM_IS_FULL = 403;
    public final static int NEW_WEBUNITJOINED = 103;
    public final static int CREATE_SALON = 104;
    private int code;
    private String owner;
    private String message;

    public WebMessage() {
    }

    public WebMessage(int code, String owner, String message) {
        this.code = code;
        this.owner = owner;
        this.message = message;
    }

    public WebMessage(int code, String message) {
        this.code = code;
        this.message = message;
        this.owner = "";
    }

    public static WebMessage parse(String message) {
        try {
            String args[] = args = message.split("\\|");
            WebMessage webMessage = new WebMessage(Integer.parseInt(args[0]), args[1], args[2]);
            return webMessage;
        }catch(Exception ex){
            System.out.println("Error! Invalid message syntax for: [ " + message + "]");
            return null;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }



    @Override
    public String toString() {
        return (code + "|" + owner +"|" + message);
    }
}
