package com.mwang.model;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.*;
import java.io.*;

public class Client extends WebUnit {
    private Socket clientSocket;    //Connection socket

    private MessageProcessor messageProcessor;

    public Client(String ip, String alias, int port) {
        this.ip = ip;
        this.alias = alias;
        this.port = port;
    }

    public Client(String ip, String alias) {
        this(ip, alias, 5555);
    }

    public Client(String alias) {
        this("127.0.0.1", alias, 5555);
    }

    public Client() {
        this("127.0.0.1", "", 5555);
    }


    //GETTER SETTERS
    public Socket getClientSocket() {
        return clientSocket;
    }

    public void setClientSocket(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public MessageProcessor getMessageProcessor() {
        return messageProcessor;
    }

    public void setMessageProcessor(MessageProcessor messageProcessor) {
        this.messageProcessor = messageProcessor;
    }

    public boolean isConnected() {
        return clientSocket != null;
    }

    //CLIENT FUNCTIONNALITIES
    public void send(int code, String content) {
        WebMessage message = new WebMessage(code, this.alias, content);
        os.print(message);
        os.flush();
    }

    public void send(WebMessage message) {
        System.out.println(message);
        os.print(message);
        os.flush();
    }

    public int connect() {
        int code = WebMessage.INTERNAL_SERVER_PROBLEM;
        if (!isConnected()) {
            try {
                this.setStatus("SEARCHING");
                this.clientSocket = new Socket(ip, port);

                this.is = new BufferedInputStream(clientSocket.getInputStream());
                this.os = new PrintWriter(clientSocket.getOutputStream());

                this.setStatus("CONNECTION");
                WebMessage message = this.read();
                if (message.getCode() == WebMessage.ROOM_IS_FULL) { //403
                    code = WebMessage.ROOM_IS_FULL;
                    clientSocket.close();
                    clientSocket = null;
                    setStatus("DISCONNECT");
                } else if (message.getCode() == WebMessage.OK) { //200
                    code = 200;
                    setStatus("CONNECTED");
                    this.messageProcessor = new MessageProcessor(this);
                    this.messageProcessor.start();
                    this.send(WebMessage.OK, this.alias);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("NO SERVER FOUND");
            }
        }
        return code;
    }

    public WebMessage read(){
        if (!this.isConnected() || is == null) {
            return null;
        }
        byte buffer[] = new byte[500];    //buffer de lecture
        String msg;
        try {
            is.read(buffer);
        } catch (SocketException ex) {
            System.out.println("Error! Socket closed"); // ERRRO IS PRESENT WHEN
        }catch(Exception ex){
            System.out.println("Error! " + ex + "while reading message for Client");
        }
        msg = (new String(buffer)).trim();
        buffer = null;
        //PREVENT PARSING A "" IN WEBMESSAGE (CAUSED BY INT.PARSING value : "")
        if (msg.equals("")) {
            return null;
        }
        WebMessage message = WebMessage.parse(msg);
        String lineIcon = "> ";
        String text = "";
        switch (message.getCode()) {
            case WebMessage.NEW_WEBUNITJOINED:
                if (!message.getMessage().equals(this.alias)) {
                    text = "Bienvenue " + message.getMessage() + " dans " + message.getOwner();
                }
                break;
            case WebMessage.CONTINUE:
                if (message.getOwner().equals(this.alias)) {
                    text = lineIcon + message.getMessage();
                } else {
                    text = message.getOwner() + lineIcon + message.getMessage();
                }
                break;
            case WebMessage.QUIT:
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                clientSocket = null;
                setStatus("DISCONNECTED");

            default:
                text = message.getMessage();
        }
        setChanged();
        notifyObservers(text);
        return message;
    }

    public boolean disconnect() {
        if (!isConnected()) {
            return true;
        }
        try {

            this.send(new WebMessage(WebMessage.QUIT, this.alias, this.alias));
            messageProcessor.interrupt();
            clientSocket.close();
            clientSocket = null;
            this.setStatus("DISCONNECT");
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
}