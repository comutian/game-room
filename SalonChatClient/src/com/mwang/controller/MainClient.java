package com.mwang.controller;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mwang.model.Client;
import com.mwang.model.WebMessage;
import com.mwang.view.menu.MenuAide;
import com.mwang.view.menu.MenuDemarrer;
import com.mwang.view.menu.MenuSalon;
import com.mwang.view.menu.MenuTaches;
import com.mwang.view.panel.MainPanel;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class MainClient extends JFrame {
    //MODEL
    Client cl;
    //VIEW
    MainPanel mp;

    //MENU EVENT HANDLER
    ActionListener mml = new MainMenuListener();

    //Menus:
    JMenuBar mBar = new JMenuBar();
    MenuDemarrer mDemarrer = new MenuDemarrer();
    MenuTaches mTaches = new MenuTaches();
    MenuAide mAide = new MenuAide();
    MenuSalon mSalon = new MenuSalon();

    /**
     * @throws java.awt.HeadlessException
     */
    public MainClient() throws HeadlessException {
        this("");
    }

    /**
     * @param
     * @throws java.awt.HeadlessException
     */
    public MainClient(String titre) throws HeadlessException {
        super(titre);
        setSize(600, 600);
        setLocationRelativeTo(null);

        mBar.add(mDemarrer);
        mBar.add(mTaches);
        mBar.add(mAide);//**

        setJMenuBar(mBar);

        mDemarrer.setMenuListener(mml);
        mTaches.setMenuListener(mml);
        mAide.setMenuListener(mml);
        mp = new MainPanel();
        getContentPane().add(mp);

        cl = new Client();
        cl.addObserver(mp);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new MainClient("Labo1 ver 2.0 ").setVisible(true);
    }

    class MainMenuListener implements ActionListener {
        public void actionPerformed(ActionEvent evt) {
            String action = evt.getActionCommand();
            if (action.equals("Se connecter")) {
                String a;
                while ((a = JOptionPane.showInputDialog(MainClient.this, "Votre alias pour cette connexion :", cl.getAlias())) != null && a.equals("")) {
                    JOptionPane.showMessageDialog(MainClient.this, "L'alias ne peut être vide");
                }

                cl.setAlias(a);

                int code = cl.connect();
                if (code == WebMessage.ROOM_IS_FULL) {
                    JOptionPane.showMessageDialog(MainClient.this, "Connexion impossible, la salle de chat est pleine");
                    return;
                } else if (code == WebMessage.INTERNAL_SERVER_PROBLEM) {
                    JOptionPane.showMessageDialog(MainClient.this, "Connexion impossible, le serveur n'est pas accessible");
                    return;
                }

                mBar.add(mSalon,2);
                mSalon.setMenuListener(mml);

                setTitle(getTitle() + " - " + cl.getAlias());
                Object source = evt.getSource();
                if (source instanceof JMenuItem) {
                    ((JMenuItem) source).setText("Se deconnecter");
                    ((JMenuItem) source).setActionCommand("Se deconnecter");
                }
            } else if (action.equals("Se deconnecter")) {
                if (!(JOptionPane.showConfirmDialog(MainClient.this, "Se deconnecter ?", "", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)) {
                    return;
                }
                if (!cl.disconnect()) {
                    JOptionPane.showMessageDialog(MainClient.this, "Deconnexion impossible");
                    return;
                }

                String titre = getTitle();
                int j = titre.indexOf("-");
                if (j > 0) {
                    setTitle(titre.substring(0, j - 1));
                }

                Object source = evt.getSource();
                if (source instanceof JMenuItem) {
                    ((JMenuItem) source).setText("Se connecter");
                    ((JMenuItem) source).setActionCommand("Se connecter");
                }
                //JOptionPane.showMessageDialog(MainClient.this,"vous êtes deconnectés");
            } else if (action.equals("Configurer serveur")) {
                JPanel p1 = new JPanel(new GridLayout(2, 1));
                JPanel p2 = new JPanel(new FlowLayout(FlowLayout.RIGHT)),
                        p3 = new JPanel(new FlowLayout(FlowLayout.RIGHT));

                JTextField t1 = new JTextField(cl.getIp(), 10);
                p2.add(new JLabel("Adresse du serveur : "));
                p2.add(t1);
                JTextField t2 = new JTextField(String.valueOf(cl.getPort()), 10);
                p3.add(new JLabel("Port du serveur : "));
                p3.add(t2);
                p1.add(p2);
                p1.add(p3);
                int r = JOptionPane.showConfirmDialog(MainClient.this, p1, "Configurer serveur", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                if (r == JOptionPane.YES_OPTION) {
                    cl.setIp(t1.getText());
                    cl.setPort(Integer.parseInt(t2.getText()));
                }
            } else if (action.equals("Quitter")) {
                if (!(JOptionPane.showConfirmDialog(MainClient.this, "Quitter ?", "", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)) {
                    return;
                }
                cl.disconnect();
                System.exit(0);
            }else if(action.equals("Quitter ce salon")){
                cl.send(new WebMessage(WebMessage.QUIT,cl.getAlias(),cl.getAlias()));
            }

            else if(action.equals("Créer un salon")){
                JPanel p1 = new JPanel(new GridLayout(1, 1));
                JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
                JTextField t1 = new JTextField(10);
                JLabel labelNom = new JLabel("Nom du salon");
                p2.add(labelNom);
                p2.add(t1);
                p1.add(p2);
                int r = JOptionPane.showConfirmDialog(MainClient.this, p1, "Créer un nouveau salon", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                if (r == JOptionPane.YES_OPTION) {
                    cl.send(new WebMessage(WebMessage.CREATE_SALON,cl.getAlias(),t1.getText()));
                }
            }
        }
    }
}