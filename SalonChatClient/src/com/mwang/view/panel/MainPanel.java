/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mwang.view.panel;

import javax.swing.*;
import javax.swing.border.*; //pour les bordures

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import com.mwang.model.Client;

public class MainPanel extends JPanel implements Observer,ActionListener,KeyListener{
    Client client;
    JPanel centerPanel;

    JTextField tfInput;
    JTextArea taRoom;

    public MainPanel() {
        super();
        centerPanel = new JPanel();
        Border blackBorder = BorderFactory.createLineBorder(Color.BLACK, 2);

        //Input Area
        tfInput = new JTextField(10);
        tfInput.addKeyListener(this);
        tfInput.setBorder(blackBorder);

        //Chatroom Area
        taRoom = new JTextArea(20, 10);
        taRoom.setEditable(false);

        centerPanel.setLayout(new BorderLayout());
        centerPanel.add(tfInput, BorderLayout.SOUTH);
        centerPanel.add(new JScrollPane(taRoom), BorderLayout.CENTER);

        this.setLayout(new BorderLayout());
        this.add(centerPanel);
        setVisible(false);
    }

    public void update(Observable o, Object t) {
        if (o instanceof Client) {
            if (client == null) {
                client = (Client) o;
                setVisible(true);
            }
            if(!t.toString().equals(""))
                taRoom.append(t.toString() + '\n');
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            String s = tfInput.getText();
            if (!s.equals("")) {
                client.send(100, s);
                tfInput.setText("");
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
