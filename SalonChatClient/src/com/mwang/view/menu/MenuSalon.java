package com.mwang.view.menu;

import com.mwang.view.menu.UnMenu;
import com.mwang.view.menu.UnMenuItem;

import javax.swing.*;

public class MenuSalon extends UnMenu {
    JMenuItem miCreate,miInvite,miQuitter;


    public MenuSalon() {
        setText("Salon");
        setActionCommand("Salon");
        setMnemonic((int) 'S');
        miCreate = new UnMenuItem("Créer un salon", 'C', mil, true);
        miQuitter = new UnMenuItem("Quitter ce salon", 'Q', mil, true);

        add(miCreate);
        addSeparator();
        add(miQuitter);
    }
}
